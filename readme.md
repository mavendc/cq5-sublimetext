# Cscope
A plugin to post to CQ5, using sublime text. Leverages the SlingPostServlet (as documented here)
http://sling.apache.org/documentation/bundles/manipulating-content-the-slingpostservlet-servlets-post.html.

## Features
This plugin supports most file types that will be PUT or POST-ed to CQ5.

1. jsp's / other scripting languages
2. 'plain' xml files
3. static files
4. design_dialog.xml
5. dialog.xml

TODO --> add .content.xml, cq_edit config.xml support
TODO --> add HTTPS support

## Installation
Check out the repo under your "Packages" directory or install via [Package Control](https://sublime.wbond.net/) and restart Sublime Text 3. Please note, Sublime Text 2 is not supported at this time 
The package control name is 'CQ5-Saver'

## Logging
Upon successful save to CQ5, the console will log the destination path and the corresponding status code response from the server to the std out. (press ctrl + tilde)
- 204 : No Content (successful)
- 201 : Content created (succesful)

## Configuration
If you wish to change the way CscopeSublime behaves, you may pass arguments to override any of the following default variables

    host = "http://localhost:4502/"
    username = 'admin'
    password = 'admin'

    COFFEE_SUFFIX = ".coffee"
    JS_SUFFIX = ".js"
    XML_SUFFIX = ".xml"
    COFFEXEC = 'coffee.cmd'
    JCRROOT = "jcr_root\\"
    ENC = 'utf-8'
    DIALOG = "dialog.xml"
    DESIGN_DIALOG = "design_dialog.xml"
    JCR_CONTENT = ".content.xml"
    CLIENTLIB = "clientlibs"
    TIMEOUT = 2

Syntax for the args can be seen in the .sublime-commands file located in this repo (i.e.) -->
"args": {"host": "http://localhost:4503/", "username" : "admin", "password" : "admin"}


## Keybindings
- `Ctrl/ + Shift + s`  - Deploy to CQ5 on localhost:4502 with basic auth (admin:admin)

## Coffee compilation
This plugin will compile 'on the fly', bare javascript from your .coffee files, directly to the corresponding JCR path


## Notes
The plugin looks within the path for a folder labeled "jcr_root\". This can be overriden by passing in an arg "JCRROOT", in case your specific folder structure does not match jcr_root\apps\yourapp\blablabla


## Bugs / Features / Forking
By all means, please fork this repo and update the code base if you find any room for improvement, of which I'm sure there is plenty.
This is my first stab at using python. Please drop a bug in the issue tracker if you find any problems.

## License
This whole package is distributed under the MIT license.